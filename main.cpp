#include <iostream>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <vector>
using namespace std;

#define LN10 2.30259
//Se define ln(10) para utilizar con los números despues de haber sido "minimizados"
//Funciones u operaciones utilizadas por los demás métodos
/*---------------------------------------------------------------------------------*/
template<class T>
T biggerThanOne(int &counter, T n);

template<class T>
T smallerThanOne(int &counter, T n);

template<class T>
T minimize(int &counter, T n, bool &biggerThan);

template<class T>
T pow_(T a, int b, T total);

template<class T>
bool isValid(T n);

template<class T>
T abs_(T expr);

template<class T>
T arctanh(T n, int iteration);

template<class T>
T relativeError(T ynMinusOne, T yn);

/*---------------------------------------------------------------------------------*/
//Logaritmos
/*-----Taylor-----*/
template<class T>
T taylorSum(T n, int precision, int iteration);

template<class T>
T taylorln(T n, int precision, int iteration, T sum);

template<class T>
T executeTaylorAfterMinimize(T n, int precision);

/*----Arctan1----*/
template<class T, class S>
T arctanhln1(T n, S precision);

template<class T, class S>
T arctanhSumln1(T n, S precision, int iteration, T sum);

template<class T>
T executeArctanh1AfterMinimize(T n, int precision);

/*----Arctan2----*/
template<class T, class S>
T arctanhln2(T n, S precision);

template<class T, class S>
T arctanhSumln2(T n, S precision, int iteration, T sum);

template<class T>
T executeArctanh2AfterMinimize(T n, int precision);

/*-------------------------------------------------------------------------------*/
//Llama todos los logaritmos con el número deseado
template<class T>
int execute(T n);

/*-------------------------------------------------------------------------------*/
//Solicita la entrada al usuario, solo realiza la operación en un número
void executeWithInput();

/*-------------------------------------------------------------------------------*/
//Imprime un mensaje de salida y retorna
void exit_();

/*-------------------------------------------------------------------------------*/
//Viaja por el vector y ejecuta la operación en cada número
template<class T>
void readFromFileAndExecute(ifstream &fileReader, int precision, T precisionln2);

/*-------------------------------------------------------------------------------*/
/*Trata de abrir el archivo antes de pasarlo a ejecutar
 *Donde:
 *      argc[1] es el objeto tipo ifstream que recorrerá el archivo
 *      argc[2] es la presición deseada para el ln1
 *      argc[3] es la presición deseada para el ln2
 */
template<class T>
void prepareFile(string fileName, int precision, T precisionln2);
/*-------------------------------------------------------------------------------*/


//
/*-----------------------------------MAIN----------------------------------------*/
int main(int argv, char ** argc){
    (argv == 4) ? prepareFile(string(argc[1]), stoi(argc[2]), strtod(argc[3], NULL)) :
    executeWithInput();    
    return 0;
}

template<class T>
T biggerThanOne(int &counter, T n){
    counter++;
    return (n/10 > 1) ? biggerThanOne(counter, n/10) : n;
}

template<class T>
T smallerThanOne(int &counter, T n){
    counter++;
    return (n*10 < 1) ? smallerThanOne(counter, n*10) : n;
}

template<class T>
T minimize(int &counter, T n, bool &biggerThan){
    biggerThan = n > 1;
    return (biggerThan)? biggerThanOne(counter, n) : smallerThanOne(counter, n);
}

template<class T>
T pow_(T a, int b, T total){
    return (b > 0) ? pow_(a, b-1, total*a) : total;
}

template<class T>
bool isValid(T n){
    return (!isinf(n) && !isnan(n)) ? true : false;
}

template<class T>
T abs_(T expr){
    return (expr<0) ? expr*-1.0 : expr;
}

template<class T>
T arctanh(T n, int iteration){
    return ((T)1/((((T)2)*iteration) +(T)1.0))
        * pow_(((n-(T)1.0)/(n+((T)1.0))),((((T)2.0)*iteration)+(T)1.0), (T)1);
}

template<class T>
T relativeError(T ynMinusOne, T yn){
    return abs_(((T)ynMinusOne-(T)yn)/(T)yn);
}

/*---------------------------------ARCTANH1------------------------------------------*/
template<class T, class S>
T arctanhSumln1(T n, S precision, int iteration, T sum){
    return (iteration<=precision) ?
        arctanhSumln1(n, precision, iteration+1, sum+(arctanh(n,iteration)))
        : sum;
}

template<class T, class S>
T arctanhln1(T n, S precision){
    return (n > 1) ? 2*arctanhSumln1(n, precision, 0, (T)0) : 0;
}

template<class T>
T executeArctanh1AfterMinimize(T n, int precision){
    bool biggerThan;
    int counter = 0;
    T newN = minimize(counter, n, biggerThan);
    counter--;
    return(biggerThan) ? (arctanhln1(newN, precision)) + (counter*LN10) :
    (arctanhln1(newN, precision)) - (counter*LN10);  
}

/*----------------------------------ARCTANH2-------------------------------------------*/

template<class T, class S>
T arctanhSumln2(T n, S precision, int iteration, T sum){
    return (relativeError(arctanh(n, iteration), arctanh(n, iteration+1)) > precision) ?
        arctanhSumln2(n, precision, iteration + 1, sum + (arctanh(n, iteration)))
       : sum;
}

template<class T, class S>
T arctanhln2(T n, S precision){
    return (n > 1) ? 2*arctanhSumln2(n, precision, 0, (T)0) : 0;
}

template<class T>
T executeArctanh2AfterMinimize(T n, int precision){
    bool biggerThan;
    int counter = 0;
    T newN = minimize(counter, n, biggerThan);
    counter--;
    return(biggerThan) ? (arctanhln2(newN, precision)) + (counter*LN10) :
    (arctanhln2(newN, precision)) - (counter*LN10);  
}
/*----------------------------------TAYLOR------------------------------------------*/
template<class T>
T taylorSum(T n, int precision, int iteration){
    T result = ( ((T)(pow_((T)-1, (T)(iteration -1) ,(T)1)) * (T)(pow_((T)(n-1), (T)iteration, (T)1))) / (T)iteration);
    return (isValid(result)) ? result : 0;
}

template<class T>
T taylorln(T n, int precision, int iteration, T sum){
    return (iteration <= precision) ?
      taylorln(n, precision, iteration + 1, sum + taylorSum(n, precision, iteration)) : sum;
}

template<class T>
T executeTaylorAfterMinimize(T n, int precision){
    bool biggerThan;
    int counter = 0;
    T newN = minimize(counter, n, biggerThan);
    counter--;
    return(biggerThan) ? (taylorln(newN, precision, 0, (T)0)) + (counter*LN10) :
    (taylorln(newN, precision,0, (T)0)) - (counter*LN10);  
}

/*-----------------------------------------------------------------------------------*/
template<class T>
void execute(T n, int precision, T precisionln2){  
    (n >0 )? void() : exit_();
    try{
        cout << "CMATH:ln("<< n << ")=" << log(n) << endl;
        cout << "TAYLOR:ln(" << n << ")=" << executeTaylorAfterMinimize(n, precision)  << endl;
        cout << "ARTANH:ln1("<< n << ")=" << executeArctanh1AfterMinimize(n, precision) << endl;
        cout << "ARTANH:ln2("<< n << ")=" << executeArctanh2AfterMinimize(n, precisionln2) << "\n" << endl;
    }
    catch(exception &e){
        cerr << e.what() << endl;}
}

void exit_(){
     cout << "Saliendo" << endl;
     exit(0);
 }
 
void executeWithInput(){
    cout.precision(6);
    double input;
    int precision;
    double precisionln2;
    cout << ">Numero a calcular: ";
    cin>>input;
    cin.clear();
    cout << ">Precision deseada: ";
    cin>>precision;
    cin.clear();
    cout << ">Precision para el error relativo: ";
    cin>>precisionln2;
    cin.clear();
    (input>0) ?
    execute(input, precision, precisionln2)
    : exit_();
}

template<class T>
void prepareFile(string fileName, int precision, T precisionln2){
    ifstream fileReader(fileName);
    (fileReader.is_open()) ? readFromFileAndExecute(fileReader, precision, precisionln2) : exit_();
    fileReader.close();
}

template<class T>
void readFromFileAndExecute(ifstream &fileReader, int precision, T precisionln2){
    string line;
    getline(fileReader, line);
    double n = strtod(line.c_str(), NULL);
    execute(n, precision, precisionln2);
    (!fileReader.eof()) ? readFromFileAndExecute(fileReader, precision, precisionln2)  : void();
}

